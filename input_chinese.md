可以采用compositionstart和compositionend来捕获IME（input method editor）的启动和关闭事件。说实话，这两事件听都没听过，但是我们还是写个demo来试试吧。

复制代码
$("#input").on(
    compositionstart:function(){
        $("#result").text("中文输入：开始")
    },
    compositionend:function(){
        $("#result").text("中文输入：结束")
    })